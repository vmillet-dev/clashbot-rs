use std::process::{Command, Stdio};
use clap::{Parser};
use clashbot_rs::adb::AdbTcpConnection;
use clashbot_rs::cli::Cli;
use clashbot_rs::event_handler::EventHandler;
use clashbot_rs::image_util::ImageUtil;
use clashbot_rs::models::TemplateMatchingRef;

fn main() {
    start_adb_server();

    let opt = Cli::parse();
    let serial = opt.device.serial.unwrap_or(
        format!("{:?}:{:?}", opt.device.device_ip.unwrap(), opt.device_port.unwrap())
    );

    let mut adb = AdbTcpConnection::new(opt.address, opt.port).unwrap();

    if opt.pairing_port.is_some() {
        adb.pair(
            opt.code.unwrap(),
            opt.device.device_ip.unwrap(),
            opt.pairing_port.unwrap()
        ).expect("TODO: panic message");

    }

    if opt.device.device_ip.is_some() {
        adb.connect(opt.device.device_ip.unwrap(), opt.device_port.unwrap()).expect("TODO: panic message");
    }

    let mut event = EventHandler::new(adb, serial);
    let screenshot = event.screenshot();
    // let image = image::open("E:/Images/Saved Pictures/template.jpg").unwrap();

    let positions = ImageUtil::find_coordinates_by_template(&screenshot, TemplateMatchingRef::ShopButton);
    println!("{:?}", positions);
}

fn start_adb_server() -> bool {
    Command::new("adb").arg("start-server").stdout(Stdio::null()).status().is_ok()
}