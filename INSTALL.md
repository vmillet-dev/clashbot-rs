# Install

You can directly download binaries in the release section for Windows / linux target or compile from sources if you wish.


## Install ADB

### Linux

```bash
sudo apt update
sudo apt install adb -y
mkdir $HOME/adb
cd $HOME/adb
wget -c https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip platform-tools-latest-linux.zip 
cd platform-tools/
export PATH=${PATH}:~/adb/platform-tools
source $HOME/.bashrc
adb version
```

### Windows

## Install Rust

### Linux

```bash
sudo apt update
sudo apt upgrade
sudo apt install build-essential curl
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
source $HOME/.cargo/env
rustc --version
```

## Compile from source

### Linux

```bash
sudo apt update
sudo apt install libssl-dev pkg-config -y
```