use std::net::Ipv4Addr;
use clap::{Args, Parser};

/// A simple bot that do stuff for you and free your time for the real world
/// Connect to the device either by an IP address over TCP or by a serial id.
/// Note: A device over TCP can be reached by serial once it has been connected to ADB (with IP:PORT as serial)
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
pub struct Cli {
    /// Sets the listening ip address of ADB server
    #[arg(short, long, default_value = "127.0.0.1")]
    pub address: Ipv4Addr,

    /// Sets the listening port of ADB server
    #[arg(short, long, default_value = "5037")]
    pub port: u16,

    #[command(flatten)]
    pub device: Device,

    /// Sets the listening port of device
    #[arg(short = 'P', long, group = "connect")]
    pub device_port: Option<u16>,

    /// Sets the listening port for pairing a new device
    #[arg(short = 'r', long, requires = "pair")]
    pub pairing_port: Option<u16>,

    /// Sets the code to pair a new device
    #[arg(short, long, group = "pair")]
    pub code: Option<u32>,

    #[command(flatten)]
    pub features: Features
}

#[derive(Args, Debug)]
#[group(required = true, multiple = false)]
pub struct Device {
    /// Sets the serial id of a specific device
    #[arg(short, long)]
    pub serial: Option<String>,

    /// Sets the ip of a specific device
    #[arg(short, long, requires = "connect")]
    pub device_ip: Option<Ipv4Addr>,
}

#[derive(Args, Debug)]
pub struct Features {
    #[arg(long("skip-tutorial"), default_value = "false")]
    pub skip_tutorial: bool
}