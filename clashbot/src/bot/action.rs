pub enum Action {
    OpenGameActivity,
    WaitForCompleteLoading,
    SkipTutorial
}