use std::thread::sleep;
use std::time::Duration;
use log::{debug, info};
use rand::Rng;

use crate::adb::AdbTcpConnection;
use crate::bot::action::Action;
use crate::bot::action::Action::{ OpenGameActivity, WaitForCompleteLoading, SkipTutorial};
use crate::bot::condition::Condition;
use crate::bot::condition::Condition::{Empty, FeatureEnabled};
use crate::bot::state::State;
use crate::bot::state::State::{ActivityNotStarted, HomeVillage, Loading};
use crate::cli::Features;
use crate::event_handler::EventHandler;
use crate::image_util::ImageUtil;

struct Decision {
    pub(self) condition: Condition,
    pub(self) action: Action,
    pub(self) states: Vec<State>
}

pub struct Bot {
    pub(crate) event_handler: EventHandler,
    pub(crate) current_state: State,
    pub(crate) features: Features,

    decision_tree: Vec<Decision>
}

impl Bot {
    pub fn new(adb: AdbTcpConnection, serial: String, features: Features) -> Bot {
        info!("Bot started");
        debug!("Features enabled: {:?}", features);
        let decision_tree = Self::build_decision_tree(&features);
        let event_handler = EventHandler::new(adb, serial);
        Bot { current_state: ActivityNotStarted, event_handler, decision_tree, features }
    }

    pub fn run(&mut self) -> () {
        info!("Run actions loop");
        let mut idle = true;
        let mut rng = rand::thread_rng();
        loop {
            if let Some(action) = Self::execute_action(self) {
                idle = false;
                match action {
                    OpenGameActivity => Self::open_app(self),
                    WaitForCompleteLoading => Self::wait_for_loading(self),
                    SkipTutorial => Self::skip_tutorial(),
                }
            } else if !idle {
                idle = true;
                info!("No action possible at the moment, standby...");
            }
            sleep(Duration::from_millis(rng.gen_range(250..1000)));
        }
    }

    fn build_decision_tree(features: &Features) -> Vec<Decision> {
        return vec![
            Decision { condition: Empty, action: OpenGameActivity, states: vec![ ActivityNotStarted ] },
            Decision { condition: Empty, action: WaitForCompleteLoading, states: vec![ Loading ] },
            Decision { condition: FeatureEnabled(features.skip_tutorial), action: SkipTutorial, states: vec![ HomeVillage ] },
        ]
    }

    fn execute_action(&mut self) -> Option<&Action> {
        for decision in &self.decision_tree {
            if Condition::evaluate_condition(&decision.condition) && decision.states.contains(&self.current_state) {
                return Some(&decision.action);
            }
        }
        None
    }
}

