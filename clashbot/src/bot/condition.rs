pub enum Condition {
    Empty,
    FeatureEnabled(bool)
}

impl Condition {
    pub fn evaluate_condition(condition: &Condition) -> bool {
        match condition {
            Condition::Empty => true,
            Condition::FeatureEnabled(value) => *value
        }
    }
}
