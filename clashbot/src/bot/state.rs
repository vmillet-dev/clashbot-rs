#[derive(PartialEq, Debug)]
pub enum State {
    ActivityNotStarted,
    Loading,
    HomeVillage,
    BuilderVillage
}