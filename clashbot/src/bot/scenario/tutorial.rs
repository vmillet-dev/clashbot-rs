use log::info;
use crate::bot::bot::Bot;

impl Bot {
    pub(crate) fn skip_tutorial() -> () {
        info!("[ACTION] Start skipping tutorial")
    }
}