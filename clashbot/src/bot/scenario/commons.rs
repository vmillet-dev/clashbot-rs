use std::thread::sleep;
use std::time::Duration;
use log::{debug, error, info};
use crate::bot::bot::Bot;
use crate::bot::state::State::{HomeVillage, Loading};
use crate::CONFIG;
use crate::image_util::ImageUtil;
use crate::models::TemplateMatchingRef;

impl Bot {
    pub(crate) fn open_app(&mut self) -> () {
        info!("[ACTION] Opening app on the device");
        let package = CONFIG.game.package_name.clone();
        let activity = CONFIG.game.activity_name.clone();

        debug!("Package name: {}", package);
        debug!("Activity name: {}", activity);

        match self.event_handler.open_app(package, activity) {
            Ok(..) => debug!("App successfully started"),
            Err(error) => {
                error!("Cannot open activity: {}", error);
                panic!("{}", error);
            }
        }

        debug!("Move current state to loading");
        self.current_state = Loading;
    }

    pub(crate) fn wait_for_loading(&mut self) -> () {
        info!("[ACTION] Wait for game to be fully loaded");

        let screenshot = self.event_handler.screenshot();
        if let Some(positions) = ImageUtil::find_coordinates_by_template(&screenshot, TemplateMatchingRef::ShopButton) {
            debug!("Position of button: {:?}", positions);
            info!("Game loaded, move current state to loading");
            self.current_state = HomeVillage;
        } else {
            debug!("Game not loaded yet");
        }
    }
}