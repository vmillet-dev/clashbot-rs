use std::fs::File;
use lazy_static::lazy_static;
use crate::models::Config;

pub mod adb;
pub mod event_handler;
pub mod image_util;
pub mod models;
pub mod yolov8;
pub mod cli;
pub mod bot;

lazy_static! {
    static ref CONFIG: Config  = serde_yaml::from_reader::<_, Config>(File::open("config/app.yaml").unwrap()).expect("Config file failed to load !");
}