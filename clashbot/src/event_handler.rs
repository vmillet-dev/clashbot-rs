use image::DynamicImage;
use image::ImageFormat::Png;
use log::debug;
use crate::adb::{AdbTcpConnection};
use crate::adb::Result;


#[derive(Debug)]
pub struct EventHandler {
    adb: AdbTcpConnection,
    serial: String,
}

impl EventHandler {
    pub fn new(adb: AdbTcpConnection, serial: String) -> Self {
        Self { adb, serial }
    }

    pub fn screenshot(&mut self) -> DynamicImage {
        let result = self.adb.shell_command(&Some(self.serial.as_str()), vec!["screencap", "-p"]).unwrap();
        image::load_from_memory_with_format(result.as_slice(), Png).expect("Could not load image from memory")
    }

    pub fn tap(&mut self, x: u16, y: u16) -> Result<Vec<u8>> {
        return self.adb.shell_command(&Some(self.serial.as_str()), vec!["input", "tap", x.to_string().as_str(), y.to_string().as_str()])
    }

    pub fn open_app(&mut self, package_name: String, main_activity_name: String) -> Result<Vec<u8>> {
        let package_ref = format!("{}/{}", package_name, main_activity_name);
        let command = vec!["am", "start", "-n", &*package_ref];

        debug!("shell command [{}]: {:?}", self.serial.as_str(), &command);
        return self.adb.shell_command(&Some(self.serial.as_str()), command)
    }

    pub fn close_app(&mut self, package_name: String) -> Result<Vec<u8>> {
        return self.adb.shell_command(&Some(self.serial.as_str()), vec!["am", "force-stop", package_name.as_str()])
    }
}