use std::process::{Command, Stdio};
use std::thread::sleep;
use std::time::Duration;
use clap::{Parser};
use log4rs::init_file;
use log::{debug, error, info};

use clashbot_rs::adb::{AdbTcpConnection, RustADBError};
use clashbot_rs::adb::RustADBError::{ADBNotFound, ADBPairingError};
use clashbot_rs::bot::bot::Bot;
use clashbot_rs::cli::Cli;

fn main() -> Result<(), RustADBError> {
    init_file("config/log4rs.yaml", Default::default()).unwrap();

    if !is_adb_installed() {
        error!("ADB is not installed");
        return Err(ADBNotFound);
    }
    start_adb_server();

    let opt = Cli::parse();
    let serial = opt.device.serial.unwrap_or(
        format!("{:?}:{:?}", opt.device.device_ip.unwrap(), opt.device_port.unwrap())
    );
    debug!("Serial: {}", serial);

    let mut adb = AdbTcpConnection::new(opt.address, opt.port).unwrap();

    if opt.pairing_port.is_some() {
        info!("Start pairing device on port {:?} with code {:?}", opt.pairing_port.unwrap(), opt.code.unwrap());
        match adb.pair(
            opt.code.unwrap(),
            opt.device.device_ip.unwrap(),
            opt.pairing_port.unwrap()
        ) {
            Ok(..) => info!("Paired device successfully"),
            Err(error) => {
                error!("{}", error);
                return Err(ADBPairingError);
            }
        };

    }

    if opt.device.device_ip.is_some() {
        info!("Start connecting device {:?} on port {:?}", opt.device.device_ip.unwrap(), opt.device_port.unwrap());
        match adb.connect(opt.device.device_ip.unwrap(), opt.device_port.unwrap()) {
            Ok(..) => info!("Device {:?} connected !", opt.device.device_ip.unwrap()),
            Err(error) => {
                match error {
                    ADBPairingError => error!("Attempting to connect on the device without being paired, you must pair before connecting"),
                    _ => error!("{}", error)
                }
                return Err(error)
            }
        }
    }

    let mut bot = Bot::new(adb, serial, opt.features);
    bot.run();

    Ok(())
}

fn is_adb_installed() -> bool {
    Command::new("adb").arg("--version").stdout(Stdio::null()).status().is_ok()
}

fn start_adb_server() -> bool {
    Command::new("adb").arg("start-server").stdout(Stdio::null()).status().is_ok()
}
