use std::{fs, io};
use std::error::Error;
use std::fs::File;
use std::path::Path;
use image::{DynamicImage, GrayImage, Rgb, RgbImage};
use image::imageops::{FilterType, resize};
use imageproc::drawing::draw_hollow_rect_mut;
use imageproc::map::map_colors;
use imageproc::rect::Rect;
use imageproc::template_matching::{Extremes, find_extremes, match_template_parallel, MatchTemplateMethod};
use ocrs::{OcrEngine, OcrEngineParams};
use rten::Model;
use rten_tensor::{NdTensor, NdTensorView};
use rten_tensor::prelude::*;

use crate::CONFIG;
use crate::models::{TemplateMatchingRef};
use crate::yolov8::{YoloArgs, YOLOv8};

pub struct ImageUtil {
    ocr_engine: OcrEngine,
    yolo_engine: YOLOv8
}

impl ImageUtil {

    pub fn new() -> ImageUtil {
        let detection_model_data = Self::find_and_update_model("text-detection.rten").unwrap();
        let rec_model_data = Self::find_and_update_model("text-recognition.rten").unwrap();

        let detection_model = Model::load(&detection_model_data).unwrap();
        let recognition_model = Model::load(&rec_model_data).unwrap();

        let ocr_engine = OcrEngine::new(OcrEngineParams {
            detection_model: Some(detection_model),
            recognition_model: Some(recognition_model),
            ..Default::default()
        }).unwrap();

        let args = YoloArgs::new();
        let yolo_engine = YOLOv8::new(args).unwrap();

        ImageUtil { ocr_engine, yolo_engine }
    }

    pub fn find_text_lines(&mut self, image: &RgbImage) -> Vec<String> {
        let image = Self::read_image(&image).unwrap();
        let ocr_input = self.ocr_engine.prepare_input(image.view()).unwrap();
        let word_rects = self.ocr_engine.detect_words(&ocr_input).unwrap();
        let line_rects = self.ocr_engine.find_text_lines(&ocr_input, &word_rects);
        let line_texts = self.ocr_engine.recognize_text(&ocr_input, &line_rects).unwrap();

        let mut res = vec![];
        for line in line_texts
            .iter()
            .flatten()
            .filter(|l| l.to_string().len() > 1)
        {
            res.push(line.to_string());
        }
        return res;
    }

    pub fn find_coordinates_by_template(image: &DynamicImage, template: TemplateMatchingRef) -> Option<(u32, u32, u32, u32)> {
        let image_grayed = image.to_luma8();
        let nw = image_grayed.width();
        let image_grayed_resized = resize(&image_grayed, nw , image_grayed.height() * nw / image_grayed.width(), FilterType::Nearest);
        let template_image = Self::find_template_image(template);

        let result = match_template_parallel(&image_grayed_resized, &template_image, MatchTemplateMethod::SumOfSquaredErrorsNormalized);

        let positions = find_extremes(&result);
        if cfg!(debug_assertions) {
            Self::draw_green_rect(&image_grayed_resized, positions, template_image.width(), template_image.height());
            println!("{:?}", positions);
        }

        if positions.min_value > 0.001 {
            return None
        }

        let (width, height) = (template_image.width(), template_image.height());
        let (x, y) = (positions.min_value_location.0 + width / 2, positions.min_value_location.1 + height / 2);

        return Some((x, y, width, height));
    }

    fn find_template_image(template: TemplateMatchingRef) -> GrayImage {
        let template_path = format!("assets/templates/{}", template.as_str());
        let path = Path::new(&template_path);

        return image::open(path)
            .unwrap_or_else(|_| panic!("Could not load template at {:?}", path))
            .to_luma8();

    }

    fn draw_green_rect(image: &GrayImage, positions: Extremes<f32>, width: u32, height: u32) -> () {
        let output_dir = Path::new("log/templates_matching");

        if !output_dir.is_dir() {
            fs::create_dir(output_dir).expect("Failed to create output directory")
        }

        let rect = Rect::at(
            positions.min_value_location.0 as i32,
            positions.min_value_location.1 as i32
        ).of_size(width, height);

        let mut color_image = map_colors(image, |p| Rgb([p[0], p[0], p[0]]));
        draw_hollow_rect_mut(&mut color_image, rect, Rgb([0, 255, 0]));

        let source_path = output_dir.join("image.png");
        color_image.save(&source_path).unwrap();
    }

    fn find_and_update_model(path: &str) -> Result<Vec<u8>, io::Error> {
        let assets_dir = Path::new("assets/ocrs");
        if !assets_dir.exists() {
            fs::create_dir_all(&assets_dir)?;
        }

        let file_path = assets_dir.join(path);

        if !file_path.exists() {
            Self::download_file(&path)
        }

        return fs::read(file_path);
    }

    fn download_file(filename: &str) {
        let mut resp = reqwest::blocking::get(format!("{}{}", CONFIG.ocr.url.as_str(), filename)).expect("request failed");
        let mut out = File::create(format!("assets/ocrs/{filename}")).expect("failed to create file");
        io::copy(&mut resp, &mut out).expect("failed to copy content");
    }

    fn read_image(input_img: &RgbImage) -> Result<NdTensor<f32, 3>, Box<dyn Error>> {
        let (width, height) = input_img.dimensions();
        let layout = input_img.sample_layout();

        let chw_tensor = NdTensorView::from_data_with_strides(
            [height as usize, width as usize, 3],
            input_img.as_raw().as_slice(),
            [
                layout.height_stride,
                layout.width_stride,
                layout.channel_stride,
            ],
        )?
            .permuted([2, 0, 1]) // HWC => CHW
            .to_tensor() // Make tensor contiguous, which makes `map` faster
            .map(|x| *x as f32 / 255.); // Rescale from [0, 255] to [0, 1]

        Ok(chw_tensor)
    }
}