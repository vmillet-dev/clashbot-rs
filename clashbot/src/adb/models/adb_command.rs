use super::RebootType;

pub enum AdbCommand {
    ConnectDevice(String, String),
    DisconnectDevice(String, String),
    DisconnectAnyDevices,
    PairDevice(String, String, String),
    Version,
    Kill,
    Devices,
    DevicesLong,
    TrackDevices,
    HostFeatures,
    TransportAny,
    TransportSerial(String),
    ShellCommand(String),
    Sync,
    Reboot(RebootType),
}

impl ToString for AdbCommand {
    fn to_string(&self) -> String {
        match self {
            AdbCommand::ConnectDevice(addr, port) => format!("host:connect:{addr}:{port}"),
            AdbCommand::DisconnectDevice(addr, port) => format!("host:disconnect:{addr}:{port}"),
            AdbCommand::DisconnectAnyDevices => "host:disconnect:".into(),
            AdbCommand::PairDevice(code, addr, port) => format!("host:pair:{code}:{addr}:{port}"),
            AdbCommand::Version => "host:version".into(),
            AdbCommand::Kill => "host:kill".into(),
            AdbCommand::Devices => "host:devices".into(),
            AdbCommand::DevicesLong => "host:devices-l".into(),
            AdbCommand::Sync => "sync:".into(),
            AdbCommand::TrackDevices => "host:track-devices".into(),
            AdbCommand::TransportAny => "host:transport-any".into(),
            AdbCommand::TransportSerial(serial) => format!("host:transport:{serial}"),
            AdbCommand::ShellCommand(command) => match std::env::var("TERM") {
                Ok(term) => format!("shell,TERM={term},raw:{command}"),
                Err(_) => format!("shell,raw:{command}"),
            },
            AdbCommand::HostFeatures => "host:features".into(),
            AdbCommand::Reboot(reboot_type) => {
                format!("reboot:{reboot_type}")
            }
        }
    }
}
