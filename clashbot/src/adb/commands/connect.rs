use std::net::Ipv4Addr;
use crate::adb::{AdbTcpConnection};
use crate::adb::models::AdbCommand;
use crate::adb::RustADBError::{ADBConnectError, ADBPairingError};

impl AdbTcpConnection {
    /// Connect adb to a device
    pub fn connect(&mut self, address: Ipv4Addr, port: u16) -> crate::adb::Result<()> {
        self.new_connection()?;

        let response = self.proxy_connection(AdbCommand::ConnectDevice(address.to_string(), port.to_string()), true)
            .map_err(|_| ADBConnectError)?;

        let response_str = String::from_utf8(response).map_err(|_| ADBConnectError)?;

        if response_str.contains("connected to") {
            self.new_connection()?;
            Ok(())
        } else if response_str.contains("failed to connect to") {
            Err(ADBPairingError)
        } else {
            Err(ADBConnectError)
        }
    }
}