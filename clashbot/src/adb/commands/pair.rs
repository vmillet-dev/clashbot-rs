use std::net::Ipv4Addr;
use crate::adb::AdbTcpConnection;
use crate::adb::models::AdbCommand;

impl AdbTcpConnection {
    /// Connect adb to a device
    pub fn pair(&mut self, code: u32, address: Ipv4Addr, port: u16) -> crate::adb::Result<()> {
        self.new_connection()?;
        self.proxy_connection(AdbCommand::PairDevice(code.to_string(), address.to_string(), port.to_string()), true)?;
        self.new_connection()?;
        Ok(())
    }
}