use std::net::Ipv4Addr;
use crate::adb::AdbTcpConnection;
use crate::adb::models::AdbCommand;

impl AdbTcpConnection {
    /// Disconnect a device from adb
    pub fn disconnect(&mut self, address: Ipv4Addr, port: u16) -> crate::adb::Result<()> {
        self.new_connection()?;
        self.proxy_connection(AdbCommand::DisconnectDevice(address.to_string(), port.to_string()), false)?;

        Ok(())
    }

    /// Disconnect any devices from adb
    pub fn disconnect_any_devices(&mut self) -> crate::adb::Result<()> {
        self.new_connection()?;
        self.proxy_connection(AdbCommand::DisconnectAnyDevices, false)?;

        Ok(())
    }
}