use std::io::{Read};

use crate::adb::{
    models::{AdbCommand, HostFeatures},
    AdbTcpConnection, Result, RustADBError,
};

impl AdbTcpConnection {
    /// Runs 'command' in a shell on the device, and return its output and error streams.
    pub fn shell_command<S: ToString>(
        &mut self,
        serial: &Option<S>,
        command: impl IntoIterator<Item = S>,
    ) -> Result<Vec<u8>> {
        let supported_features = self.host_features(serial)?;
        if !supported_features.contains(&HostFeatures::ShellV2)
            && !supported_features.contains(&HostFeatures::Cmd)
        {
            return Err(RustADBError::ADBShellNotSupported);
        }

        self.new_connection()?;

        match serial {
            None => self.send_adb_request(AdbCommand::TransportAny)?,
            Some(serial) => {
                self.send_adb_request(AdbCommand::TransportSerial(serial.to_string()))?
            }
        }
        self.send_adb_request(AdbCommand::ShellCommand(
            command
                .into_iter()
                .map(|v| v.to_string())
                .collect::<Vec<_>>()
                .join(" "),
        ))?;

        const BUFFER_SIZE: usize = 512;
        let result = (|| {
            let mut result = Vec::new();
            loop {
                let mut buffer = [0; BUFFER_SIZE];
                match self.tcp_stream.read(&mut buffer) {
                    Ok(size) => {
                        if size == 0 {
                            return Ok(result);
                        } else {
                            result.extend_from_slice(&buffer[..size]);
                        }
                    }
                    Err(e) => {
                        return Err(RustADBError::IOError(e));
                    }
                }
            }
        })();

        self.new_connection()?;
        result
    }
}
