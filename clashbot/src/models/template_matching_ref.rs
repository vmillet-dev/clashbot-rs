pub enum TemplateMatchingRef {
    RedCrossButton,
    UpgradeButton,
    GoldCoin,
    ShopButton
}

impl TemplateMatchingRef {
    pub fn as_str(&self) -> &str {
        match self {
            TemplateMatchingRef::RedCrossButton => "redcross_button.jpg",
            TemplateMatchingRef::UpgradeButton => "upgrade_button.jpg",
            TemplateMatchingRef::GoldCoin => "gold_coin.jpg",
            TemplateMatchingRef::ShopButton => "shop_button.jpg",
        }
    }
}