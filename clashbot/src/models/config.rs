use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub ocr: Ocr,
    pub game: Game
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Ocr {
    pub url: String,
    pub auto_update: bool
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Game {
    pub package_name: String,
    pub activity_name: String
}
